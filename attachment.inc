<?php

// This driver contributed by WorldFallz (http://drupal.org/user/155304)

/**
 * Each module that manages files needs to have a 'driver' like this one, containing the three functions below,
 * with the module's name (a.k.a 'the current module' or 'the file management module' below) as the second segment 
 * of the function name, e.g., get_attachment_file_list(). More documentation is available at 
 * http://interoperating.info/mark/search_attachments.
 */

/**
 * Given a node ID, returns a nested array which is a list of files attached to the node which are managed 
 * by the current module, and their attributes as stored by the current module ('id', 'path', and 'driver').
 */
function get_attachment_file_list($nid) {
  $result = db_query("SELECT fid, hidden FROM {attachment} WHERE nid = %d", $nid);
  while ($row = db_fetch_object($result)) {
    if (!$row->hidden) {
      $attachments[] = array('id' => $row->fid, 'path' => filemanager_create_path($row->fid), 'driver' => 'attachment');
    }
  }
  return $attachments;
}

/**
 * Given a file's ID in the current module's db table, returns an array of attributes ('link', 'name', 'size', 'nid').
 */
function get_attachment_file($fid) {
  $result = db_query("SELECT aid, nid, filename, size, hidden FROM {attachment} WHERE fid = %d", $fid);
  $file = db_fetch_object($result);
  $info = array();

  if ( ($file->nid) && (!$file->hidden) ){
      $info['link'] = filemanager_url(filemanager_get_file_info($fid), FALSE, TRUE);
      $info['name'] = $file->filename;
      $info['size'] = $file->size;
      $info['nid'] = $file->nid;
  }
  return $info;
}

/**
 * Returns a permission string that controls who can view files managed by the current module. This string should
 * be identical to the one used by the file management module.
 */
function get_attachment_view_permission() {
  return 'add attachments';
}
?>
