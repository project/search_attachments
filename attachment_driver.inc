<?php

/**
 * Search_attachments driver for the attachment module. Originally contributed by WorldFallz
 * (http://drupal.org/user/155304). See upload_driver.inc for more information on how drivers work.
 *
 * NOTE: This driver does not currently respect the 'hidden' attribute. This means that all users
 * can see links to all files in the search results list, although attachment.module should enforce
 * access to the contents of the file. Respect for 'hidden' in searching is currently under development.
 */
 
/**
 * Returns a nested array of files which are managed by the current module, with their 'id' and 
 * 'path' attributes as defined by the current module, and their last modified time via stat()'s
 * mtime value. Only files that have an extension defined in the {search_attachments_helpers} table 
 * get returned by this function.
 */
function search_attachments_get_attachment_register_files() {
  $result = db_query("SELECT fid, filename FROM {attachment}");
  while ($row = db_fetch_object($result)) {
    if (search_attachments_has_helper($row->filename)) {
			$nids = search_attachments_get_attachment_file_nids($row->fid);
			// {attachment} only records file names, not paths, and instead uses filemanager's path functions.
			$stats = stat(filemanager_create_path($row->fid, $working = FALSE));
			$files[] = array('module_id' => $row->fid, 'nid' => $nids, 'file_path' => 
				filemanager_create_path($row->fid, $working = FALSE), 'module' => 'attachment', 
				'changed' => $stats['mtime']);
    }
  }
  return $files;
}

/**
 * Given a file's ID in the current module's db table, returns a serialized array of all the nodes
 * the file is attached to. For attachment.module, there should always only be one nid in this list 
 * since the module allows a file to be attached to only one node.
 */
function search_attachments_get_attachment_file_nids($fid) {
  $nids = array();
  $result = db_query('SELECT nid FROM {attachment} WHERE fid = %d', $fid);
  while ($row = db_fetch_array($result)) {
    $nids[] = $row['nid'];
  }
  $serialized_nids = serialize($nids);
  return $serialized_nids;
}

/**
 * Given a file's ID in the current module's db table, returns an array of attributes ('link', 'name', 'size', 'nid').
 */
function search_attachments_get_attachment_file($fid) {
  $result = db_query("SELECT nid, filename, size FROM {attachment} WHERE fid = %d", $fid);
  $file = db_fetch_object($result);
  $info = array();

  if ($file->nid){
      $stats = stat(filemanager_create_directory_path($fid));
      $info['mtime'] = $stats['mtime'];
      $info['url'] = filemanager_url(filemanager_get_file_info($fid), FALSE, TRUE);
      $info['name'] = $file->filename;
      $info['size'] = $file->size;
      $info['nid'] = search_attachments_get_attachment_file_nids($fid);
  }
  return $info;
}

/**
 * Returns a permission string that controls who can view files managed by the current module. This string should
 * be identical to the one used by the file management module.
 */
function search_attachments_get_attachment_view_permission() {
  return 'add attachments';
}
?>
