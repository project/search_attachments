<?php

/**
 * This driver handles all files that have been uploaded to Drupal via FTP, etc., that is, not using a
 * file management module and/or not attached to nodes. If you are writing a driver for a file management 
 * module see upload_driver.inc for more information on how drivers work -- do not use this driver 
 * as an example.
 */
 
 /**
 * Returns a nested array of files which are not managed by a file mangagement module (i.e., are
 * FTPed or otherwise copied into the directories defined by the site admin in search_attachments.module's
 * admin settings).
 */
function search_attachments_get_no_file_manager_register_files() {
  $files = array();
  // First, get a list of all the file paths in {search_attachments_files} that belong to files managed
  // by other modules. This module should only return files that are not managed by other modules.
  static $paths_managed_by_other_modules = array();
  $result = db_query("SELECT file_path FROM {search_attachments_files} WHERE module != 'no_file_manager'");
  while ($row = db_fetch_array($result)) {
    $paths_managed_by_other_modules[] = $row['file_path'];
  }
  
  // Scan the directories as identifed in the admin settings
  $dirs_to_scan = variable_get('search_attachments_dirs_to_scan','');
  // Split the string into an array of directory names so we can iterate through them
  $dirs_to_scan = preg_split('/(\r\n?|\n)/', $dirs_to_scan);
  // We only want directories that contain some non-whitespace character (in other words, that aren't blank)
  $dirs_to_scan = preg_grep('/\S/', $dirs_to_scan);
  // Remove duplicates caused by child directories that are listed being included in recursion of
  // parents, etc. NOTE: This does not dedupe child directories, so we still need to dedupe files below.
  $dirs_to_scan = array_unique($dirs_to_scan);
  // Initialize an array to keep track of files so we don't add the same file twice
  $seen_files = array();
  
  foreach ($dirs_to_scan as $directory) {
    // print "Directory is $directory<p />";
    $directory = trim($directory);
    $recurse = FALSE;
    // If the user has included the trailing /, recurse the directory
    if (preg_match('/\/$/', $directory)) {
      $recurse = TRUE;
      // Remove the trailing slash since we don't want it in the file path, we just use it as an
      // indicator to recurse the directory.
      $directory = rtrim($directory, '/');
    }
    // Get files within the current directory
		$scanned_files = file_scan_directory($directory, $mask = '.', $nomask = array('.', '..', 'CVS'), 
			$callback = 0, $recurse, $key = 'filename', $min_depth = 0, $depth = 0);
		foreach ($scanned_files as $file) {
		  if (!in_array($file->filename, $seen_files) && !in_array($file->filename, $paths_managed_by_other_modules) && is_file($file->filename)) {
		    if (search_attachments_has_helper($file->filename)) {
					$stats = stat($file->filename);
					// print "File is " . $file->filename . '<p />';
					// We give files a module_id of 0 for now; one will be assigned when its row is inserted within
					// search_attachments_register_files($op = 'update')
					$files[] = array('module_id' => '0', 'nid' => '0', 'file_path' => trim($file->filename), 
						'module' => 'no_file_manager', 'changed' => $stats['mtime']);
						// Add the file path to the list of ones we check before adding entries, to avoid duplication
					$seen_files[] = $file->filename;
		    }
		  }
		}
  }
  
  // Filter out file paths that match one of the exclusion patterns
  // print '$files = <pre>'; print_r($files); print '</pre>';
  $files_filtered = search_attachments_filter_file_paths($files);
  // Remove all the elements that were given null values in search_attachments_filter_file_paths()
  foreach ($files_filtered as $ff_key => $ff_value) {
    if (is_null($ff_value)) unset($files_filtered[$ff_key]);
  }
  // print '$files_filtered = <pre>'; print_r($files_filtered); print '</pre>';
  return $files_filtered;
}

/**
 * Tests each file path to see if it matches one of the patterns that exclude files from being 
 * registered by this driver. Only returns files with paths that do not match any of the patterns.
 */
function search_attachments_filter_file_paths(&$files) {
  // Patterns to skip as identified in the admin settings
  $patterns_to_skip = variable_get('search_attachments_paths_to_not_scan', '');
  // Split the string into an array of regular expressions so we can iterate through them
  $patterns_to_skip = preg_split('/(\r\n?|\n)/', $patterns_to_skip);
  // We only want patterns that contain some non-whitespace character (in other words, that aren't blank)
  $patterns_to_skip = preg_grep('/\S/', $patterns_to_skip);
  for ($i = 0; $i < count($files); $i++) {
		foreach ($patterns_to_skip as $pattern) {
			$pattern = trim($pattern);
			// If the file path matches one of the patterns, delete the entire $file element
			// if (preg_match($pattern, $files_to_keep[$i]['file_path'])) {
			if (preg_match($pattern, $files[$i]['file_path'])) {
				// print "Pattern is <strong>" . $pattern . '</strong>, file path <strong>' . $files[$i]['file_path'] . "</strong> matches and will be excluded<p />";
				// print 'Want to delete file ' . $i . '<pre>'; print_r($files[$i]); print '</pre>';
				// Can't use unset() on variable passed by reference
				$files[$i] = null;
			}
		}
	}
	return $files;
}


/**
 * Given a file's ID in the current module's db table, returns an array of attributes ('link', 'name', 
 * 'size', 'nid'). This module uses {search_attachments_files} since there is no "current module's db table".
 */
function search_attachments_get_no_file_manager_file($fid) {
  $path = db_result(db_query("SELECT file_path FROM {search_attachments_files} WHERE module = 
    'no_file_manager' AND module_id = %d", $fid));
  $info = array();
  if ($path) { // Can't use $file->nid like in other drivers because it is 0
    $info['url'] = file_create_url($path);
    // Get the name from the path
    $info['name'] = substr($path, strrpos($path,'/')+1, strlen($path) - strrpos($path,'/'));
    // Use stat() to get the file's size
    $stats = stat($path);
    $info['size'] = $stats['size'];
    $info['mtime'] = $stats['mtime'];
    $info['nid'] = '0';
  }
  return $info;
}

/**
 * In other drivers, returns a permission string that controls who can view files managed by the 
 * current module. Since there is no module to define the permission string, we simply return TRUE
 * here. That means that everyone can see these files in search results. In a later version, we will 
 * use the {search_attachments_files}.include_in_index field to allows admins to exclude some files 
 * from appearing in search results for all users.
 */
function search_attachments_get_no_file_manager_view_permission() {
  return TRUE;
}

?>
