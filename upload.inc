<?php

/**
 * Each module that manages files needs to have a 'driver' like this one, containing the three functions below,
 * with the module's name (a.k.a 'the current module' or 'the file management module' below) as the second segment 
 * of the function name, e.g., get_upload_file_list(). More documentation is available at 
 * http://interoperating.info/mark/search_attachments.
 */

/**
 * Given a node ID, returns a nested array which is a list of files attached to the node which are managed 
 * by the current module, and their attributes as stored by the current module ('id', 'path', and 'driver').
 */
function get_upload_file_list($nid) {
  $result = db_query("SELECT fid, filepath FROM {files} WHERE nid = '%d'", $nid);
  while ($row = db_fetch_object($result)) {
      $attachments[] = array('id' => $row->fid, 'path' => $row->filepath, 'driver' => 'upload');
  }
  return $attachments;
}

/**
 * Given a file's ID in the current module's db table, returns an array of attributes ('link', 'name', 'size', 'nid').
 */
function get_upload_file($fid) {
  // Select the parent node's ID, the attachment's file name, link and size
  $result = db_query("SELECT nid, filesize as size, filename as name, filepath as path FROM {files} WHERE fid = '%d'", $fid);
  $file = db_fetch_object($result);
  $info = array();
  if ($file->nid) {
    $info['link'] = file_create_url($file->path);
    $info['name'] = $file->name;
    $info['size'] = $file->size;
    $info['nid'] = $file->nid;
  } 
  return $info;
}

/**
 * Returns a permission string that controls who can view files managed by the current module. This string should
 * be identical to the one used by the file management module.
 */
function get_upload_view_permission() {
  // Return permission string from module that allows users to access attachments. 
  return 'view uploaded files';
}
?>
