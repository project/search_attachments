<?php

/**
 * Each module that manages files needs to have a 'driver' like this one, containing the four functions below,
 * each with the module's name (a.k.a 'the current module' or 'the file management module' below) as the 
 * second segment of the function name, e.g., get_upload_file_list(). More documentation is available at 
 * http://interoperating.info/mark/search_attachments.
 */

/**
 * Returns a nested array of files which are managed by the current module, with their 'id' and 
 * 'path' attributes as defined by the current module, and their last modified time via stat()'s
 * mtime value. We serialize $row->nid because nids are stored in {search_attachments_files} in
 * serialized in order to accommodate arrays of nids for some file management modules, e.g., webfm.
 * Only files that have an extension defined in the {search_attachments_helpers} table get returned
 * by this function.
 */
function search_attachments_get_upload_register_files() {
  $result = db_query("SELECT fid, filepath FROM {files}");
  while ($row = db_fetch_object($result)) {
    if (search_attachments_has_helper($row->filepath)) {
			$stats = stat($row->filepath);
			$nids = search_attachments_get_upload_file_nids($row->fid);
			$files[] = array('module_id' => $row->fid, 'nid' => $nids, 'file_path' => $row->filepath, 
				'module' => 'upload', 'changed' => $stats['mtime']);
    }
  }
  return $files;
}

/**
 * Given a file's ID in the current module's db table, returns an array of attributes ('link', 'name', 
 * 'size', 'nid'). We serialize $row->nid because nids are stored in {search_attachments_files} in
 * serialized in order to accommodate arrays of nids for some file management modules, e.g., webfm.
 */
function search_attachments_get_upload_file($fid) {
  // Select the parent node's ID, the attachment's file name, link and size
  $result = db_query("SELECT filesize AS size, filename AS name, filepath AS path FROM {files} 
    WHERE fid = '%d'", $fid);
  $file = db_fetch_object($result);
  $info = array();
	$stats = stat($file->path);
	$info['mtime'] = $stats['mtime'];
	$info['url'] = file_create_url($file->path);
	$info['name'] = $file->name;
	$info['size'] = $file->size;
  $info['nid'] = search_attachments_get_upload_file_nids($fid);
  return $info;
}

/**
 * Given a file's ID in the current module's db table, returns a serialized array of all the nodes
 * the file is attached to. For upload.module, there should always only be one nid in this list since
 * upload.module allows a file to be attached to only one node.
 */
function search_attachments_get_upload_file_nids($fid) {
  $nids = array();
  $result = db_query('SELECT nid FROM {files} WHERE fid = %d', $fid);
  while ($row = db_fetch_array($result)) {
    $nids[] = $row['nid'];
  }
  $serialized_nids = serialize($nids);
  return $serialized_nids;
}

/**
 * Returns a permission string that controls who can view files managed by the current module. 
 * This string should be identical to the one used by the file management module.
 */
function search_attachments_get_upload_view_permission() {
  // Return permission string from module that allows users to access attachments. 
  return 'view uploaded files';
}
?>
