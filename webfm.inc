<?php

/**
 * Each module that manages files needs to have a 'driver' like this one, containing the three functions below,
 * with the module's name (a.k.a 'the current module' or 'the file management module' below) as the second segment
 * of the function name, e.g., get_webfm_file(). More documentation is available at 
 * http://interoperating.info/mark/search_attachments.
 */

/**
 * Given a node ID, returns a nested array which is a list of files attached to the node which are managed
 * by the current module, and their attributes as stored by the current module ('id', 'path', and 'driver').
 */
function get_webfm_file_list($nid) {
  // Select all webfm.module filepaths associated with the node
  $result = db_query("SELECT {webfm_file}.fid, fpath as filepath FROM {webfm_file}, {webfm_attach} 
    WHERE {webfm_attach}.fid = {webfm_file}.fid AND {webfm_attach}.nid = '%d'", $nid);
  while ($row = db_fetch_object($result)) {
    $attachments[] = array('id' => $row->fid, 'path' => $row->filepath, 'driver' => 'webfm');
  }
  return $attachments;
}

/**
 * Given a file's ID in the current module's db table, returns an array of attributes ('link', 'name', 'size', 'nid'). 
 */
function get_webfm_file($fid) {
  // Select the parent node's ID, the attachment's file name, link and size 
  $result = db_query('SELECT * FROM {webfm_file} f INNER JOIN {webfm_attach} a ON f.fid = a.fid WHERE a.fid = %d', $fid);
  $file = db_fetch_object($result);
  $info = array();    
  if($file->nid) {
     global $base_url;
     $info['link'] = $base_url . '/?q=webfm_send/' . $file->fid;
     $info['name'] = $file->fname;
     $info['size'] = $file->fsize;
     $info['nid'] = $file->nid;
     return $info;
  }
}

/**
 * Returns a permission string that controls who can view files managed by the current module. This string should
 * be identical to the one used by the file management module.
 */
function get_webfm_view_permission() {
  // Return permission string from module that allows users to access attachments.
  return 'view webfm_attachments';
}
?>
