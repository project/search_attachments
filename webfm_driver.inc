<?php

/**
 * Search_attachments driver for the webfm module. See upload_driver.inc for more information on
 * how drivers work.
 */
 
 /**
 * Returns a nested array of files which are managed by the current module, with their 'id' and 
 * 'path' attributes as defined by the current module, and their last modified time via stat()'s
 * mtime value. Since a file managed by webfm can be attached to more than one node, we do two
 * queries, the first to get the file attributes and the second to get an array of nids the file
 * is attached to. We then return a single associative array combining all the information about all
 * the files, each file entry containing a serialized list of the nids it is attached to.
 * Only files that have an extension defined in the {search_attachments_helpers} table get returned
 * by this function.
 */
function search_attachments_get_webfm_register_files() {
  $result = db_query("SELECT fid, fpath FROM {webfm_file}");
  while ($row = db_fetch_object($result)) {
    if (search_attachments_has_helper($row->fpath)) {
			$stats = stat($row->fpath);
			$nids = search_attachments_get_webfm_file_nids($row->fid);
			$files[] = array('module_id' => $row->fid, 'file_path' => $row->fpath, 'module' => 'webfm', 
				'changed' => $stats['mtime'], 'nid' => $nids);
    }
  }
  return $files;
}

/**
 * Given a file's ID in the current module's db table, returns an array of attributes ('link', 'name', 
 * 'size', 'nid'). Since a file managed by webfm can be attached to more than one node, we do two
 * queries, the first to get the file attributes and the second to get an array of which nids the file
 * is attached to. We then return a single associative array combining all the information about this
 * file, with the array of nids serialized.
 */
function search_attachments_get_webfm_file($fid) {
  // Select the file's file name, link and size, and use stat to get last modified time
  $result = db_query('SELECT fname, fsize FROM {webfm_file} WHERE fid = %d', $fid);
  $file = db_fetch_object($result);
  $info = array();    
	global $base_url;
	$stats = stat($file->fpath);
	$info['mtime'] = $stats['mtime'];
	// Is there a webfm function to do this?
	$info['url'] = $base_url . '/?q=webfm_send/' . $file->fid;
	$info['name'] = $file->fname;
	$info['size'] = $file->fsize;
  $info['nid'] = search_attachments_get_webfm_file_nids($fid);
  return $info;
}

/**
 * Given a file's ID in the current module's db table, returns a serialized array of all the nodes
 * the file is attached to. For webfm.module, there may be more than one nid in this list since
 * webfm allows a file to be attached to multiple nodes.
 */
function search_attachments_get_webfm_file_nids($fid) {
  $nids = array();
  $result = db_query('SELECT nid FROM {webfm_attach} WHERE fid = %d', $fid);
  while ($row = db_fetch_array($result)) {
    $nids[] = $row['nid'];
  }
  $serialized_nids = serialize($nids);
  return $serialized_nids;
}

/**
 * Returns a permission string that controls who can view files managed by the current module. 
 * This string should be identical to the one used by the file management module.
 */
function search_attachments_get_webfm_view_permission() {
  // Return permission string from module that allows users to access attachments.
  return 'view webfm attachments'; 
}
?>
